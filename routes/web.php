<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/data-table', function () {
    return view('halaman.datatable');
});

Route::get('/table', function () {
    return view('halaman.table');
});

//CRUD CAST
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast/store', 'CastController@store');
Route::get('/cast/edit/{id}', 'CastController@edit');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('cast/{id}', 'CastController@destroy');
